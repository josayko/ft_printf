/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 14:20:31 by jonny             #+#    #+#             */
/*   Updated: 2020/01/02 11:33:43 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/libftprintf.h"
#include <stdio.h>

int		main(void)
{
	char *str = "Salut";
	int ret = 0;
	void *fun = ft_printf;
	int nb = 2147483647;
	int neg = -2147483647;

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 00: flag c *****\n");
	printf("ft_printf:\n");
	ret = ft_printf ("multiple char => |%c%c%c%c%c|", 'h', 'e', 'l', 'l', 'o');
	printf("	|ret = %d|\n", ret);
	printf("printf:\n");
	ret = printf ("multiple char => |%c%c%c%c%c|", 'h', 'e', 'l', 'l', 'o');
	printf("	|ret = %d|\n\n", ret);

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 01: flag s *****\n");
	printf("ft_printf:\n");
	ret = ft_printf("str => |%s|", str);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{-8s} => |%-8s|", str);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{10s} => |%10s|", str);
	printf("	|ret = %d|\n", ret);

	printf("printf:\n");
	ret = printf("str => |%s|", str);
	printf("	|ret = %d|\n", ret);
	ret = printf("{-8s} => |%-8s|", str);
	printf("	|ret = %d|\n", ret);
	ret = printf("{10s} => |%10s|", str);
	printf("	|ret = %d|\n\n", ret);

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 02: flag p *****\n");
	printf("ft_printf:\n");
	ret = ft_printf("pointer on char => |%p|", str);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{-20p} => |%-20p|", str);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("pointer on function => |%p|", fun);
	printf("	|ret = %d|\n", ret);

	printf("printf:\n");
	ret = printf("pointer on char => |%p|", str);
	printf("	|ret = %d|\n", ret);
	ret = printf("{-20p} => |%-20p|", str);
	printf("	|ret = %d|\n", ret);
	ret = printf("pointer on function => |%p|", fun);
	printf("	|ret = %d|\n\n", ret);

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 03: flags d i u  *****\n");
	printf("ft_printf:\n");
	ret = ft_printf("{d} => |%d|", nb);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{i} => |%i|", neg);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{u} => |%u|", neg);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{.5d} => |%10d|", nb);
	printf("	|ret = %d|\n", ret);

	printf("printf:\n");
	ret = printf("{d} => |%d|", nb);
	printf("	|ret = %d|\n", ret);
	ret = printf("{i} => |%i|", neg);
	printf("	|ret = %d|\n", ret);
	ret = printf("{u} => |%u|", neg);
	printf("	|ret = %d|\n", ret);
	ret = printf("{.5d} => |%10d|", nb);
	printf("	|ret = %d|\n\n", ret);

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 04: flag x X *****\n");
	printf("ft_printf:\n");
	ret = ft_printf("lowercase => |%x|", nb);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("uppercase => |%X|", nb);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{-20p} => |%-20x|", nb);
	printf("	|ret = %d|\n", ret);
	ret = ft_printf("{.20X} => |%.20X|", neg);
	printf("	|ret = %d|\n", ret);

	ret = printf("lowercase => |%x|", nb);
	printf("	|ret = %d|\n", ret);
	ret = printf("uppercase => |%X|", nb);
	printf("	|ret = %d|\n", ret);
	ret = printf("{-20p} => |%-20x|", nb);
	printf("	|ret = %d|\n", ret);
	ret = printf(" {.20X}=> |%.20X|", neg);
	printf("	|ret = %d|\n\n", ret);

	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
	printf("***** TEST 05: %% sign *****\n");
	printf("ft_printf:\n");
	ret = ft_printf ("{%%%%%%%%%%%%} => |%%%%%%|");
	printf("	|ret = %d|\n", ret);
	printf("printf:\n");
	ret = printf ("{%%%%%%%%%%%%} => |%%%%%%|");
	printf("	|ret = %d|\n\n", ret);

//	printf(" - - - - - - - - - - - - - - - - - - - - -\n");
//	printf("***** TEST 06: precision [0] & [nb] *****\n");
//	ft_printf("ft_printf:\n");
//	ft_printf("[%%05d] => |%05d|\n", nb);
//	ret = ft_printf("[%%012d] => |%012d|\n", neg);
//	printf("ret = %d\n", ret);
//	printf("printf:\n");
//	printf("[%%05d] => |%05d|\n", nb);
//	ret = printf("[%%012d] => |%012d|\n", neg);
//	printf("ret = %d\n\n", ret);
	return (0);
}
