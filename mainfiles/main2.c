/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jonny <josaykos@student.42.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/30 23:51:10 by jonny             #+#    #+#             */
/*   Updated: 2020/01/06 16:19:33 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/libftprintf.h"
#include <stdio.h>

int main(void)
{
	int ret1;
	int ret2;
	char *p = NULL;
	//char *p2 = "\0";
	ret1 = ft_printf("FbON%%03WuV9PM8s%-9s6ZFAF0\n", NULL);
	printf("%d\n", ret1);
	ret1 = ft_printf("%%%s%-9s\n", "salut", NULL);
	printf("%d\n", ret1);
	ret1 = ft_printf("-->|%-16.p|<--\n", p);
	printf("%d\n", ret1);
	ret1 = ft_printf("-->|%-16p|<--\n", p);
	printf("%d\n", ret1);
	ret1 = ft_printf("-->|%-15.p|<--\n", p);
	printf("%d\n", ret1);
	ret1 = ft_printf("-->|%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret1);
	ret1 = ft_printf("-->|%-4.2d|<--\n", -1);
	printf("%d\n", ret1);

	ret2 = printf("FbON%%03WuV9PM8s%-9s6ZFAF0\n", NULL);
	printf("%d\n", ret2);
	ret2 = printf("%%%s%-9s\n", "salut", NULL);
	printf("%d\n", ret2);
	ret2 = printf("-->|%-16.p|<--\n", p);
	printf("%d\n", ret2);
	ret2 = printf("-->|%-16p|<--\n", p);
	printf("%d\n", ret2);
	ret2 = printf("-->|%-15.p|<--\n", p);
	printf("%d\n", ret2);
	ret2 = printf("-->|%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret2);
	ret2 = printf("-->|%-4.2d|<--\n", -1);
	printf("%d\n", ret2);
	return 0;
}
