/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main3.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/06 16:28:37 by josaykos          #+#    #+#             */
/*   Updated: 2020/01/07 10:42:47 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/libftprintf.h"
#include <stdio.h>

int		main(void)
{
	int ret1;
	int ret2;
	ret1 = 0;
	ret2 = 0;

	ret1 = ft_printf("-->|%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret1);
	ret2 = printf("-->|%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret2);

	ret1 = ft_printf("-->|Salut%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret1);
	ret2 = printf("-->|Salut%*.*d|<--\n", 4, 3, -12);
	printf("%d\n", ret2);
}
