/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main4.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: josaykos <josaykos@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/06 16:42:55 by josaykos          #+#    #+#             */
/*   Updated: 2020/01/07 11:28:06 by josaykos         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../ft_printf/libftprintf.h"
#include <stdio.h>

int		main(void)
{
	int ret1;
	int ret2;
	ret1 = 0;
	ret2 = 0;

	ret1 = ft_printf("-->|%-4.2d|<--\n", -1);
	printf("%d\n", ret1);
	ret2 = printf("-->|%-4.2d|<--\n", -1);
	printf("%d\n", ret2);
//	ret1 = ft_printf("|%019.*db8vQ%.4sBCe|\n", 10, -112496761, NULL);
//	printf("%d\n", ret1);
//	ret2 = printf("|%019.*db8vQ%.4sBCe|\n", 10, -112496761, NULL);
//	printf("%d\n", ret2);
}

